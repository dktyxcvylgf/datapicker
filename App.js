/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, StatusBar, Text, Dimensions} from 'react-native';
import {
  DefaultTheme,
  Provider as PaperProvider,
  configureFonts,
} from 'react-native-paper';
import {useNetInfo} from '@react-native-community/netinfo';
import Main from './_app/Main';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'tomato',
    accent: 'yellow',
  },
};

const App = () => {
  const netInfo = useNetInfo();
  return (
    <PaperProvider theme={theme}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{flex: 1}}>
        {netInfo.isConnected ? <Main /> : <Text>no inet</Text>}
      </SafeAreaView>
    </PaperProvider>
  );
};

export default App;
