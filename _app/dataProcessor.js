import React, {useState} from 'react';
import moment from 'moment';
import {List, Title, Card, ActivityIndicator, Button} from 'react-native-paper';
import {main_url, pic_url} from './config';
import {Image, Text, View} from 'react-native';

const ImageLoader = (source) => {
  const [pic, setPic] = useState(true);
  return (
    <View>
      {pic && <ActivityIndicator animating={true} />}
      <Image
        style={{height: 50, width: 50}}
        onLoadEnd={() => setPic(false)}
        source={{uri: source.source}}
      />
    </View>
  );
};

const ListLeagues = (data) => {
  const {element, league, key} = data.data;
  const match = element[league];

  return (
    <List.Accordion
      key={key}
      id={'league' + key}
      title={league + `(${match[0].sportName})`}
      left={() => <List.Icon icon="folder" />}>
      {match.map((matchInfo, key1) => {
        return (
          <List.Accordion
            key={key1}
            id={'match' + key1}
            title={moment(matchInfo.time).format('Матч YYYY-MM-DD в HH:mm ')}>
            <List.Section>
              <List.Subheader>Играют:</List.Subheader>
              {matchInfo.teams.map((team, key2) => {
                return (
                  <List.Item
                    titleStyle={{fontWeight: '200'}}
                    left={() => (
                      <ImageLoader source={`${pic_url}${team.logo}`} />
                    )}
                    key={key2}
                    title={team.name_ru || team.name}
                  />
                );
              })}
              <Button
                onPress={() => {
                  alert(`Ставок: ${matchInfo.betsTotal}`);
                }}>
                Посмотреть ставки
              </Button>
            </List.Section>
          </List.Accordion>
        );
      })}
    </List.Accordion>
  );
};

export default Processor = ({data, date}) => {
  let filtered = [];
  for (const key in data) {
    if (moment(data[key].time).isSame(date, 'day')) {
      filtered.push(data[key]);
    }
  }

  let tournaments = [];
  tournaments.push(
    filtered.reduce((prev, now) => {
      (prev[now['tournamentName']] = prev[now['tournamentName']] || []).push(
        now,
      );
      return prev;
    }, {}),
  );

  let resComps = [];

  if (tournaments) {
    tournaments.forEach((element) => {
      Object.keys(element).forEach((league, key) => {
        resComps.push(<ListLeagues key={key} data={{element, league, key}} />);
      });
    });
  }

  if (resComps.length < 1) {
    return <Title>Данных на {moment(date).format('YYYY-MM-DD')} нет</Title>;
  } else {
    return resComps;
  }
};
