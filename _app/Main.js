import React, {useEffect, useState, lazy, Suspense} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  Platform,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import Axios from 'axios';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {List, Button} from 'react-native-paper';
import {main_url} from './config';

const Processor = lazy(() => import('./dataProcessor'));

Axios.defaults.baseURL = main_url;

const Main = () => {
  const [matches, setMatches] = useState();
  const [state, setState] = useState({data: null, loading: true});
  const [date, setDate] = useState(new Date());
  const [show, setShow] = useState(false);

  useEffect(() => {
    setState((state) => ({data: state.data, loading: true}));
    Axios.get(main_url)
      .then((result) => {
        let data = result.data;
        setState({data: data, loading: false});
      })
      .catch((error) => {
        alert(error);
        setState({data: null, loading: false});
      });
  }, [main_url]);

  const onChange = (event, selectedDate) => {
    setShow(false);
    const currentDate = selectedDate || date;
    setDate(currentDate);
  };

  const processData = () => {
    let data = state.data;
    if (data) {
      setMatches(
        data ? (
          <Suspense fallback={<ActivityIndicator />}>
            <Processor data={data} date={date} />
          </Suspense>
        ) : (
          <ActivityIndicator animating={true} color={'red'} />
        ),
      );
    }
  };

  useEffect(() => {
    processData();
  }, [date, state.data]);

  return (
    <View>
      <Button
        onPress={() => {
          setShow(!show);
        }}>
        Выберите дату
      </Button>
      {show && (
        <DateTimePicker
          value={date}
          display="calendar"
          onChange={onChange}></DateTimePicker>
      )}
      <ScrollView>{matches}</ScrollView>
    </View>
  );
};

export default Main;
